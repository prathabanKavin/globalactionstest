package com.example.globalactionstest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    MyAccessibilityService myAccessibilityService = new MyAccessibilityService();
    private Button permission_button;
    private Button home_button;
    private Button back_button;
    private Button recent_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        permission_button = findViewById(R.id.permission_button);
        home_button = findViewById(R.id.home_button);
        back_button = findViewById(R.id.back_button);
        recent_button = findViewById(R.id.recent_button);

        permission_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
            }
        });

        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAccessibilityService.getInstance().HomeKey();
            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAccessibilityService.getInstance().BackKey();
            }
        });

        recent_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAccessibilityService.getInstance().Recentskey();
            }
        });
    }
}