package com.example.globalactionstest;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.EventLogTag;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Button;
import android.widget.FrameLayout;

public class MyAccessibilityService extends AccessibilityService {

    private static MyAccessibilityService instance;
    private static final String TAG = "MyAccessibilityService";

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
    }

    @Override
    public void onInterrupt() {
        Log.e(TAG, "onInterrupt: Something went wrong");
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        instance = this;
        Log.e(TAG, "onServiceConnected: ");
    }

    public static MyAccessibilityService getInstance() {
        return instance;
    }

    public void BackKey() {
        performGlobalAction(GLOBAL_ACTION_BACK);
        Log.e(TAG, "Back key pressed: ");
    }

    public void HomeKey() {
        performGlobalAction(GLOBAL_ACTION_HOME);
        Log.e(TAG, "Home key pressed: ");
    }

    public void Recentskey() {
        performGlobalAction(GLOBAL_ACTION_RECENTS);
        Log.e(TAG, "Recents key pressed: ");
    }
}
